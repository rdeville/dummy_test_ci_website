# Configuration du CI

## Configurer le `${HOME}` du compte projet

La première chose à faire est de configurer le compte project PAGoDA pour avoir
accès à la command rrsync comme décrit dans le tutoriel de [générateur de site
static proposer par Vincent Nivoliers][static_site_gen].

Je me suis déjà chargé de mettre en place la configuration des clés du site
projet.liris.cnrs.fr dans le CI. Vous avez besoin de générer le binaire
`rrsync` dans le `${HOME}` du projet PAGoDA:

```bash
gunzip /usr/share/doc/rsync/scripts/rrsync.gz -c > /home-projets/pagoda/rrsync
chmod u+x /home-projets/pagoda/rrsync
```

## Récupérer le contenu de `authorized_keys`

Maintenant il va falloir configurer la connexion SSH pour faire en sorte que la
clé privé passwordless ne soit autorisée qu'à faire du `rsync` sur le dossier
`/home-projets/pagoda/public_html/`.

Aller dans le project [PAGoDA/PAGoDA > Settings > CI/CD][pagoda_settings_ci]
puis dans la section Variables.

Dans cette section, il y a plusieurs variables, celle qui va t'intéresser est la
variable `AUTHORIZED_KEY_CONTENT`.

Elle contient le contenu à ajouter au fichier `.ssh/authorized_keys` dans le
`${HOME}` du compte projet PAGoDA.

Vous n'avez qu'à copier/coller ce contenu dans le fichier sus-mentioné.

Normalement ça devrait être bon. Pour être sûr, vous pouvez relancer le CI sur la
branche [feature-ci][pagoda_branch_feat_ci].

[static_site_gen]: https://gitlab.liris.cnrs.fr/cell-si/documentation-si/wikis/workflows/generateurs-sites-statiques-gitlab-runners-pages-personnelles#configuration-des-cl%C3%A9s-ssh
[pagoda_settings_ci]: https://gitlab.liris.cnrs.fr/pagoda/pagoda/settings/ci_cd
[pagoda_branch_feat_ci]: https://gitlab.liris.cnrs.fr/pagoda/pagoda/tree/feature-ci